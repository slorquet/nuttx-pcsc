# PC/SC for NuttX#

This directory contains am implementation of PC/SC (or pcsclite, or winscard) API for NuttX.

Since it does not follow the nuttx coding standard, this code cannot be integrated in NuttX.

To use it, you have to clone this directory inside the apps/ repository. Note that it is NOT a submodule.

Step 1 - Go to your apps/ working copy

Step 2 - Run: git clone https://bitbucket.org/slorquet/nuttx-pcsc pcsc

Step 3 - Go to your nuttx/ working copy

Step 4 - Run: make apps_distclean then make menuconfig

Step 5 - Configure NuttX to enable the smard card reader manager (to be done) then select PC/SC applications that are available in the Applications menu.
