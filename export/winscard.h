/****************************************************************************
 * include/winscard.h
 *
 *   Copyright (C) 2018 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_WINSCARD_H
#define __INCLUDE_WINSCARD_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

/* Written by looking ONLY at reference documentation here:
 * https://pcsclite.alioth.debian.org/api/winscard_8h.html
 * Also available here:
 * https://msdn.microsoft.com/en-us/library/windows/desktop/aa374731(v=vs.85).aspx
 * The API from Microsoft is larger than pcsclite, we only implement
 * the minimum required functions.
 */

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define MAX_ATR_SIZE 33

/* protocol control information */

#define SCARD_PCI_T0    (&g_rgSCardT0Pci) 
#define SCARD_PCI_T1    (&g_rgSCardT1Pci) 

/* Error codes */

#define SCARD_S_SUCCESS             ((LONG)0x00000000) 
#define SCARD_F_INTERNAL_ERROR      ((LONG)0x80100001) 
#define SCARD_E_CANCELLED           ((LONG)0x80100002) 
#define SCARD_E_INVALID_HANDLE      ((LONG)0x80100003) 
#define SCARD_E_INVALID_PARAMETER   ((LONG)0x80100004) 
#define SCARD_E_INVALID_TARGET      ((LONG)0x80100005) 
#define SCARD_E_NO_MEMORY           ((LONG)0x80100006) 
#define SCARD_F_WAITED_TOO_LONG     ((LONG)0x80100007) 
#define SCARD_E_INSUFFICIENT_BUFFER ((LONG)0x80100008) 
#define SCARD_E_UNKNOWN_READER      ((LONG)0x80100009) 
#define SCARD_E_TIMEOUT             ((LONG)0x8010000A) 
#define SCARD_E_SHARING_VIOLATION   ((LONG)0x8010000B) 
#define SCARD_E_NO_SMARTCARD        ((LONG)0x8010000C) 
#define SCARD_E_UNKNOWN_CARD        ((LONG)0x8010000D) 
#define SCARD_E_CANT_DISPOSE        ((LONG)0x8010000E) 
#define SCARD_E_PROTO_MISMATCH      ((LONG)0x8010000F) 
#define SCARD_E_NOT_READY           ((LONG)0x80100010) 
#define SCARD_E_INVALID_VALUE       ((LONG)0x80100011) 
#define SCARD_E_SYSTEM_CANCELLED    ((LONG)0x80100012) 
#define SCARD_F_COMM_ERROR          ((LONG)0x80100013) 
#define SCARD_F_UNKNOWN_ERROR       ((LONG)0x80100014) 
#define SCARD_E_INVALID_ATR         ((LONG)0x80100015) 
#define SCARD_E_NOT_TRANSACTED      ((LONG)0x80100016) 
#define SCARD_E_READER_UNAVAILABLE  ((LONG)0x80100017) 
#define SCARD_P_SHUTDOWN            ((LONG)0x80100018) 
#define SCARD_E_PCI_TOO_SMALL       ((LONG)0x80100019) 
#define SCARD_E_READER_UNSUPPORTED  ((LONG)0x8010001A) 
#define SCARD_E_DUPLICATE_READER    ((LONG)0x8010001B) 
#define SCARD_E_CARD_UNSUPPORTED    ((LONG)0x8010001C) 
#define SCARD_E_NO_SERVICE          ((LONG)0x8010001D) 
#define SCARD_E_SERVICE_STOPPED     ((LONG)0x8010001E) 
#define SCARD_E_UNEXPECTED          ((LONG)0x8010001F) 
#define SCARD_E_UNSUPPORTED_FEATURE ((LONG)0x8010001F) 
#define SCARD_E_ICC_INSTALLATION    ((LONG)0x80100020) 
#define SCARD_E_ICC_CREATEORDER     ((LONG)0x80100021) 

/****************************************************************************
 * Type Definitions
 ****************************************************************************/

/* Opaque types whose composition is not available to the user */

struct scardcontext_s;
struct scardhandle_s;

/* Official type names */

typedef long                LONG;
typedef uint32_t            DWORD;
typedef FAR uint32_t *      LPDWORD;
typedef FAR void *          LPVOID;
typedef FAR const void *    LPCVOID;
typedef FAR char *          LPSTR;
typedef FAR const char *    LPCSTR;
typedef FAR uint8_t *       LPBYTE;
typedef FAR const uint8_t * LPCBYTE;

/* These structures are public */

struct scardreaderstate_s
{
  FAR const char *szReader;
  FAR void       *pvUserData;
  DWORD           dwCurrentState;
  DWORD           dwEventState;
  DWORD           cbAtr;
  unsigned char   rgbAtr[MAX_ATR_SIZE];
};

struct scardiorequest_s
{
  unsigned long dwProtocol;
  unsigned long cbPciLength;
};

/* Pointer types */

typedef FAR struct scardcontext_s *  SCARDCONTEXT;
typedef FAR struct scardcontext_s ** LPSCARDCONTEXT;
typedef FAR struct scardhandle_s *   SCARDHANDLE;
typedef FAR struct scardhandle_s **  LPSCARDHANDLE;
typedef struct scardreaderstate_s    SCARD_READERSTATE;
typedef struct scardiorequest_s      SCARD_IO_REQUEST;

/****************************************************************************
 * Variables
 ****************************************************************************/

/* These are used to represent smart card protocols. */

extern const SCARD_IO_REQUEST g_rgSCardT0Pci, g_rgSCardT1Pci;

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/* Context operations */

LONG SCardEstablishContext(DWORD dwScope, LPCVOID pvReserved1,
                           LPCVOID pvReserved2, LPSCARDCONTEXT phContext);

LONG SCardReleaseContext(SCARDCONTEXT hContext);

LONG SCardIsValidContext(SCARDCONTEXT hContext);

/* Card Readers */

LONG SCardConnect(SCARDCONTEXT hContext, LPCSTR szReader, DWORD dwShareMode,
                  DWORD dwPreferredProtocols, LPSCARDHANDLE phCard,
                  LPDWORD pdwActiveProtocol);

LONG SCardReconnect(SCARDHANDLE hCard, DWORD dwShareMode,
                    DWORD dwPreferredProtocols, DWORD dwInitialization,
                    LPDWORD pdwActiveProtocol);

LONG SCardDisconnect(SCARDHANDLE hCard, DWORD dwDisposition);

LONG SCardListReaders(SCARDCONTEXT hContext, LPCSTR mszGroups, LPSTR mszReaders,
                      LPDWORD pcchReaders);

LONG SCardFreeMemory(SCARDCONTEXT hContext, LPCVOID pvMem);

LONG SCardGetAttrib(SCARDHANDLE hCard, DWORD dwAttrId, LPBYTE pbAttr,
                    LPDWORD pcbAttrLen);

LONG SCardSetAttrib(SCARDHANDLE hCard, DWORD dwAttrId, LPCBYTE pbAttr,
                    DWORD cbAttrLen);

/* IO */

LONG SCardStatus(SCARDHANDLE hCard, LPSTR mszReaderName, LPDWORD pcchReaderLen,
                 LPDWORD pdwState, LPDWORD pdwProtocol, LPBYTE pbAtr,
                 LPDWORD pcbAtrLen);

LONG SCardGetStatusChange(SCARDCONTEXT hContext, DWORD dwTimeout,
                          FAR SCARD_READERSTATE *rgReaderStates, DWORD cReaders);

LONG SCardCancel(SCARDCONTEXT hContext);

LONG SCardControl(SCARDHANDLE hCard, DWORD dwControlCode, LPCVOID pbSendBuffer,
                  DWORD cbSendLength, LPVOID pbRecvBuffer, DWORD cbRecvLength,
                  LPDWORD lpBytesReturned);

LONG SCardTransmit(SCARDHANDLE hCard, FAR const SCARD_IO_REQUEST *pioSendPci,
                   LPCBYTE pbSendBuffer, DWORD cbSendLength,
                   FAR SCARD_IO_REQUEST *pioRecvPci, LPBYTE pbRecvBuffer,
                   LPDWORD pcbRecvLength);

/* Transactions */

LONG SCardBeginTransaction(SCARDHANDLE hCard);

LONG SCardEndTransaction(SCARDHANDLE hCard, DWORD dwDisposition);

#endif /* WINSCARD_H */

